<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>新規投稿</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

        <div class="form-area">
        <form action="newPost" method="post">
            件名<br />
            <textarea name="subject" cols="50" rows="1" maxlength="20" class="tweet-box"></textarea>
            <br />
            本文<br />
            <textarea name="text" cols="100" rows="5" maxlength="1000" class="tweet-box"></textarea>
            <br />
            カテゴリー
            <select name="category">
   			<option value="業務内">業務内</option>
			<option value="業務外">業務外</option>
			</select>
    <br />
            <input type="submit" value="投稿">
        </form>
        <a href="index.jsp">ホーム画面へ戻る</a>
</div>
        </div>