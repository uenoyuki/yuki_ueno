<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
</head>
<body>
	<div class="main-contents">
		<a href="signup">新規登録</a><br/><br/>
		<a href="index.jsp">戻る</a>
	</div>
	<ul class="users">
		<c:forEach items="${users}" var="user">
		<hr>
			<li class="users">
				<dl class="name">
					<dt>Name:</dt>
					<dd><c:out value="${user.name}" /></dd>
				</dl>
				<dl class="login_id">
					<dt>LoginId:</dt>
					<dd><c:out value="${user.login_id}" /></dd>
				</dl>
				<dl class="department_id">
					<dt>Dep:</dt>
					<dd><c:out value="${user.department_id}" /></dd>
				</dl>
				<dl class="branch_id">
					<dt>Bra:</dt>
					<dd><c:out value="${user.branch_id}" /></dd>
				</dl>

				<a href="setting?users=${user.id}">ユーザー情報の編集</a>

				<br/>
				<div class="form_conf">

<!-- アカウントの停止ボタン -->
				<c:if test="${ user.stop_user == 0 }"><br />
				<form action="userStop" method="post">
                <input type="hidden" id="userId" name="userId" value="${user.id}">

                <input type="submit" value="アカウント停止" />

              	</form>
              	</c:if>
<!-- アカウント復活ボタン -->
				<c:if test="${ user.stop_user == 1 }">
              	<form action="userRevival" method="post"><br />
                <input type="hidden" id="userId" name="userId" value="${user.id}">

                <input type="submit" value="アカウント復活" /> <br />

              	</form>
              	</c:if>
				</div>
			</li>

		</c:forEach>
	</ul>
</body>
</html>