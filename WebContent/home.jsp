<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
           <div class="header">

    <c:if test="${ empty loginUser }">

        <a href="login">ログイン</a>
    </c:if>
    <c:if test="${ not empty loginUser }">
        <a href="logout">ログアウト</a>
        <a href="newPost">新規投稿</a>
        <a href="Management">ユーザー管理画面</a>
    </c:if>
</div>
<c:if test="${ not empty loginUser }">
    <div class="profile">
        <div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
    </div>
</c:if>
            <div class="copyright"> Copyright(c)YourName</div>
        </div>

<!-- 編集画面へ権限がない人が遷移した際のエラー文章 -->
        <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

<!-- 投稿の表示 -->
        <ul class="posts">
   <c:forEach items="${posts}" var="posted">
   <hr>
            <li class="posted">
            	<dl class="date">
                   <dt>投稿時間:</dt>
                   <dd><fmt:formatDate value="${posted.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></dd>
                </dl>
                <dl class="name">
                   <dt>投稿者:</dt>
                   <dd><c:out value="${posted.name}" /></dd>
                </dl>
                <dl class="subject">
                   <dt>件名:</dt>
                   <dd><c:out value="${posted.subject}" /></dd>
                </dl>
                <dl class="text">
                   <dt>本文:</dt>
                   <dd><c:out value="${posted.text}" /></dd>
                </dl>
                <br>
                 <div class="category">カテゴリー：<c:out value="${posted.category}" /></div>
			<br />
            </li>
<!-- 投稿の削除 -->
			 <c:if test="${loginUser.id == posted.userId }">
			  <form action="deletePost" method="post">
			  <input type="hidden" id="postId" name="postId" value="${posted.id}">
			  <input type="submit" value="削除する">

			  </form>
			  </c:if>
<!-- エラーメッセージ -->
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="comment">
                            <li><c:out value="${comment}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>
<!-- コメント入力 -->
            <c:if test="${ not empty loginUser }">
             <form action="newComment" method="post">
            <textarea name="text" cols="100" rows="2"maxlength="500" class="comment-box"></textarea>
            <br />

            <input type="submit" value="コメントする">（500文字まで）
            <input type="hidden" id="postId" name="postId" value="${posted.id}">
			<br />


<!-- コメントの表示欄 -->
            ～コメント欄～
			<c:forEach items="${commented}" var="comment">

			<c:if test="${comment.postId == posted.id }">

     		<div class="comment">
                <div class="comment">
                    <span class="name"><c:out value="${comment.name}" /></span>
                    <span class="text"><c:out value="${comment.text}" /></span>
                    <div class="date"><fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
               </div>
            </div>
			</c:if>
<!-- コメントの削除 -->
			<br />
			</c:forEach>
        </form>
            </c:if>
    </c:forEach>
</ul>
    </body>
</html>



