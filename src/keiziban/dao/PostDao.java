package keiziban.dao;

import static keiziban.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import keiziban.beans.Post;
import keiziban.exception.SQLRuntimeException;

public class PostDao {

    public void insert(Connection connection, Post posts) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO posts ( ");//INSERT INTOでpostsテーブルに追加
            //どこのカラムの値を操作するのかを設定。
            sql.append("user_id");//
            sql.append(", subject");
            sql.append(", text");
            sql.append(", category");
            sql.append(", created_date");
            sql.append(") VALUES (");//ここから下は実際になにを入力するのか。
            sql.append(" ?"); // user_id
            sql.append(", ?"); // subject  ","は一つ前のuser_idとの区切り。user_idのほうは一番最初だから","がない。
            sql.append(", ?"); // text
            sql.append(", ?"); // text
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(")");

            //データベースに送るためにオブジェクトを生成
            ps = connection.prepareStatement(sql.toString());
            //最初の1は1つ目の？をさす。
            ps.setInt(1, posts.getUserId());
            ps.setString(2, posts.getSubject());
            ps.setString(3, posts.getText());
            ps.setString(4, posts.getCategory());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}