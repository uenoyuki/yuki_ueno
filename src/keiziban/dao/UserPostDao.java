package keiziban.dao;

import static keiziban.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import keiziban.beans.UserPost;
import keiziban.exception.NoRowsUpdatedRuntimeException;
import keiziban.exception.SQLRuntimeException;

public class UserPostDao {

    public List<UserPost> getUserPost(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.id as id, ");
            sql.append("posts.text as text, ");
            sql.append("posts.subject as subject, ");
            sql.append("posts.category as category, ");
            sql.append("posts.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("posts.created_date as created_date ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserPost> ret = toUserPostList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserPost> toUserPostList(ResultSet rs)
            throws SQLException {

        List<UserPost> ret = new ArrayList<UserPost>();
        try {
            while (rs.next()) {
                String text = rs.getString("text");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String subject = rs.getString("subject");
                String category = rs.getString("category");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserPost posted = new UserPost();
                posted.setSubject(subject);
                posted.setCategory(category);
                posted.setName(name);
                posted.setId(id);
                posted.setUserId(userId);
                posted.setText(text);
                posted.setCreated_date(createdDate);

                ret.add(posted);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public void deletePost(Connection connection, int postId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM posts");
            sql.append(" WHERE");
            //アップデートする対象をどのキーを対象とするか決める。今回の場合だとidがキーになる。
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, postId);

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}