package keiziban.dao;

import static keiziban.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import keiziban.beans.UserComment;
import keiziban.exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComment(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.text as text, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.post_id as post_id, ");
            sql.append("users.name as name, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                String text = rs.getString("text");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                int postId = rs.getInt("post_id");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserComment commented = new UserComment();
                commented.setName(name);
                commented.setId(id);
                commented.setUserId(userId);
                commented.setPostId(postId);
                commented.setText(text);
                commented.setCreated_date(createdDate);

                ret.add(commented);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}