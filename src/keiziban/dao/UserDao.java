package keiziban.dao;

import static keiziban.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import keiziban.beans.User;
import keiziban.exception.NoRowsUpdatedRuntimeException;
import keiziban.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", name");
            sql.append(", password");
            sql.append(", department_id");
            sql.append(", branch_id");
            sql.append(") VALUES (");
            sql.append("?"); // ログインID
            sql.append(", ?"); // name
            sql.append(", ?"); // password
            sql.append(", ?"); // department_id
            sql.append(", ?"); // branch_id
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getName());
            ps.setString(3, user.getPassword());
            ps.setInt(4, user.getDepartment_id());
            ps.setInt(5, user.getBranch_id());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String name = rs.getString("name");
                String password = rs.getString("password");
                int department_id = rs.getInt("department_id");
                int branch_id = rs.getInt("branch_id");
                int stop_user = rs.getInt("stop_user");

                User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);
                user.setName(name);
                user.setPassword(password);
                user.setDepartment_id(department_id);
                user.setBranch_id(branch_id);
                user.setStop_user(stop_user);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public User getUser(Connection connection, String login_id,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ?  AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, login_id);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public List<User> getUsers(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();

            sql.append("select * from users");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> ret = toUserList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public User getUser3(Connection connection,int id){

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ? ";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);//toUserList(rs)はretがもどされる
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  login_id = ?");
            sql.append(", name = ?");
            sql.append(", password = ?");
            sql.append(", department_id = ?");
            sql.append(", branch_id = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            //アップデートする対象をどのキーを対象とするか決める。今回の場合だとidがキーになる。
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getName());
            ps.setString(3, user.getPassword());
            ps.setInt(4, user.getDepartment_id());
            ps.setInt(5, user.getBranch_id());
            ps.setInt(6, user.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
    public void userStop(Connection connection, int userId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append(" stop_user = 1");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, userId);


            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
    public void userRevival(Connection connection, int userId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append(" stop_user = 0");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, userId);


            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

 }
