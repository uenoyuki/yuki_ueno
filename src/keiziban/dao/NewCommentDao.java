package keiziban.dao;

import static keiziban.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import keiziban.beans.NewComment;
import keiziban.exception.SQLRuntimeException;

public class NewCommentDao {

    public void insert(Connection connection, NewComment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");//INSERT INTOでpostsテーブルに追加
            //どこのカラムの値を操作するのかを設定。
            sql.append("user_id");//
            sql.append(", text");
            sql.append(", post_id");
            sql.append(", created_date");
            sql.append(") VALUES (");//ここから下は実際になにを入力するのか。
            sql.append(" ?"); // user_id
            sql.append(", ?"); // text  ","は一つ前のuser_idとの区切り。user_idのほうは一番最初だから","がない。
            sql.append(", ?"); // post_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(")");

            //データベースに送るためにオブジェクトを生成
            ps = connection.prepareStatement(sql.toString());
            //最初の1は1つ目の？をさす。
            ps.setInt(1, comment.getUserId());
            ps.setString(2, comment.getText());
            ps.setInt(3, comment.getPostId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}