package keiziban.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import keiziban.beans.User;
import keiziban.service.UserService;


@WebServlet(urlPatterns = { "/Management" })
public class ManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

//get送信とpost送信のイメージは相手からの要求はget、処理を渡すのはpost。
//情報を見せてくれはGET、情報をデータベースに格納してくれはPOST。

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

//権限の有無判定
    	 HttpSession session = request.getSession();

    	 List<String> messages = new ArrayList<String>();
    	User user = (User) session.getAttribute("loginUser");
    	if (user.getDepartment_id() != 1) {

                messages.add("アクセス権限がありません。");
                session.setAttribute("errorMessages", messages);
                request.getRequestDispatcher("/index.jsp").forward(request, response);
                return;
            }else {


         List<User> users = new UserService().getUser();

         request.setAttribute("users", users);
         request.getRequestDispatcher("/management.jsp").forward(request, response);
            }

    }

}
