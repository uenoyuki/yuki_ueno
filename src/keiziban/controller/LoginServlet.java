package keiziban.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import keiziban.beans.User;
import keiziban.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    //LoginServletのGET送信は特に処理は無いので"login.jsp"が表示される

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");

        //loginServiceからユーザー情報を持ってくる。情報はuserの中にまとめられている。
        LoginService loginService = new LoginService();
        User user = loginService.login(login_id, password);

        HttpSession session = request.getSession();
        if(user == null) {

            List<String> messages = new ArrayList<String>();
            messages.add("ログインに失敗しました。");
            session.setAttribute("errorMessages", messages);
            //requestは一度投げたら消えてしまうからもう一度このメソッドでsetAttributeする必要がある。
            request.setAttribute("login_id",login_id);
            //request, responseされた情報を返す。
            request.getRequestDispatcher("login.jsp").forward(request, response);

        }
        if(user.getStop_user() != 0) {
        	 List<String> messages = new ArrayList<String>();
             messages.add("このアカウントは使用できません。");
             session.setAttribute("errorMessages", messages);
             request.getRequestDispatcher("login.jsp").forward(request, response);
        } else {

            session.setAttribute("loginUser", user);
            response.sendRedirect("index.jsp");
        }
    }

}