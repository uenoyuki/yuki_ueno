package keiziban.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import keiziban.beans.User;
import keiziban.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	 HttpSession session = request.getSession();

    	 List<String> messages = new ArrayList<String>();
    	User user = (User) session.getAttribute("loginUser");
    	if (user.getDepartment_id() != 1) {

                messages.add("アクセス権限がありません。");
                session.setAttribute("errorMessages", messages);
                request.getRequestDispatcher("/index.jsp").forward(request, response);
                return;
            }else {
        request.getRequestDispatcher("signup.jsp").forward(request, response);
            }
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();

        //isValidのメソッドを呼び出している。isValidにはtrueかfalseの値が入っている。
        if (isValid(request, messages) == true) {

        	//user beansにアカウント情報を入れるために情報をまとめてセットしている。
            User user = new User();
            user.setName(request.getParameter("name"));
            user.setLogin_id(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            //getParameterした値は自動的にString型になるからInt型になおす。
            user.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));
            user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));

            //実際にアカウント情報を登録しにいく
            new UserService().register(user);

            response.sendRedirect("./");
        } else {
        	//セッションの中の"errorMessages"という項目にmessagesを入れている。
            session.setAttribute("errorMessages", messages);
            //request.setAttribute("name",request.getParameter("name"));
            setRequestParameter(request, "name");//上記や下記の文でもできるがこの文のほうが短くなる。書いているのは下記のrequest.setAttributeと同じ。
            request.setAttribute("login_id",request.getParameter("login_id"));
            request.setAttribute("Department_id",request.getParameter("department_id"));
            request.setAttribute("Branch_id",request.getParameter("branch_id"));
            //sendRedirectはGET送信
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String account = request.getParameter("login_id");
        String password = request.getParameter("password");
        String department_id = request.getParameter("department_id");
        String branch_id = request.getParameter("branch_id");


        if (StringUtils.isEmpty(account) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if (StringUtils.isEmpty(department_id) == true) {
            messages.add("部署IDを入力してください");
        }
        if (StringUtils.isEmpty(branch_id) == true) {
            messages.add("支店IDを入力してください");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    //ここでもう一度requestをいれる。上記のsetRequestParameterを使うときは下記の文がいる。
    private static void setRequestParameter(HttpServletRequest request, String itemName) {
    	request.setAttribute(itemName, request.getParameter(itemName));
    }

}