package keiziban.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import keiziban.beans.User;
import keiziban.exception.NoRowsUpdatedRuntimeException;
import keiziban.service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

//get送信とpost送信のイメージは相手からの要求はget、処理を渡すのはpost。
//情報を見せてくれはGET、情報をデータベースに格納してくれはPOST。

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	User editUser = new UserService().getUser3(Integer.parseInt(request.getParameter("users")));
        request.setAttribute("editUser", editUser);
        request.getRequestDispatcher("setting.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);

        if (isValid(request, messages) == true) {

            try {
                new UserService().update(editUser);
            } catch (NoRowsUpdatedRuntimeException e) {
                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                request.getRequestDispatcher("setting.jsp").forward(request, response);
                return;
            }

            response.sendRedirect("./Management");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            request.getRequestDispatcher("setting.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setName(request.getParameter("name"));
        editUser.setLogin_id(request.getParameter("login_id"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
        editUser.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));
        return editUser;
    }


    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String account = request.getParameter("login_id");
        String password = request.getParameter("password");

        if (StringUtils.isEmpty(account) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}