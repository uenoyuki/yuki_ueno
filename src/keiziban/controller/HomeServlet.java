package keiziban.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import keiziban.beans.UserComment;
import keiziban.beans.UserPost;
import keiziban.service.NewCommentService;
import keiziban.service.PostService;

//”@WebServlet”はWEBからの呼び出しの際に必ず必要。

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {          //下記2行は決まり文句
    private static final long serialVersionUID = 1L;

    @Override
  //webからGETの処理が入った場合にdoGetの処理が始まる。
  //HttpServletは、リクエストを受信し、レスポンスを返すための受け口となるクラス。
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<UserPost> posts = new PostService().getPost();

        request.setAttribute("posts", posts);

   //コメントの情報を持ってくる
        List<UserComment> commented = new NewCommentService().getComment();

        request.setAttribute("commented", commented);
//home.jspに投げる処理
        request.getRequestDispatcher("/home.jsp").forward(request, response);

    }
}