package keiziban.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import keiziban.beans.NewComment;
import keiziban.beans.User;
import keiziban.service.NewCommentService;

@WebServlet(urlPatterns = { "/newComment" })
public class NewCommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> comments = new ArrayList<String>();

        if (isValid(request, comments) == true) {

        	//sessionの情報を取得してuserに格納
            User user = (User) session.getAttribute("loginUser");

            NewComment comment = new NewComment();
            //massageに情報をセットしていく。まだ準備段階。
            comment.setText(request.getParameter("text"));
            comment.setUserId(user.getId());
            comment.setPostId(Integer.parseInt(request.getParameter("postId")));
            //実際に登録処理を行っていく。
            new NewCommentService().register(comment);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", comments);
            response.sendRedirect("./");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> comments) {

    	//top.jspから送られてきた"message"をString型でmessageに格納
        String comment = request.getParameter("text");

        if (StringUtils.isEmpty(comment) == true) {
            comments.add("本文を入力してください");
        }
        if (comments.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}