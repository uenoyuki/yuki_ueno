package keiziban.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import keiziban.beans.Post;
import keiziban.beans.User;
import keiziban.service.PostService;

@WebServlet(urlPatterns = { "/newPost" })
public class PostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    //doGetが無いときはエラーになる。　post.jspを開き投稿を押した瞬間にdoGetが走るのでdoGetは必ずいる。
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("post.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

        	//sessionの情報を取得してuserに格納
            User user = (User) session.getAttribute("loginUser");

            Post posts = new Post();
            //massageに情報をセットしていく。まだ準備段階。
            posts.setSubject(request.getParameter("subject"));
            posts.setText(request.getParameter("text"));
            posts.setCategory(request.getParameter("category"));
            posts.setUserId(user.getId());
            //実際に登録処理を行っていく。
            new PostService().register(posts);

            response.sendRedirect("newPost");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("./");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	//top.jspから送られてきた"message"をString型でmessageに格納
        String message = request.getParameter("subject");
        String message2 = request.getParameter("text");

        if (StringUtils.isEmpty(message) == true) {
            messages.add("件名を入力してください");
        }
        if (StringUtils.isEmpty(message2) == true) {
            messages.add("本文を入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}