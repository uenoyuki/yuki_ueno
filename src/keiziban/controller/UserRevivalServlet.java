package keiziban.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import keiziban.service.UserRevivalService;


@WebServlet(urlPatterns = { "/userRevival" })
public class UserRevivalServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

//get送信とpost送信のイメージは相手からの要求はget、処理を渡すのはpost。
//情報を見せてくれはGET、情報をデータベースに格納してくれはPOST。

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        int userId = (Integer.parseInt(request.getParameter("userId")));

        //loginServiceからユーザー情報を持ってくる。情報はuserの中にまとめられている。
        UserRevivalService UserRevivalService = new UserRevivalService();
        UserRevivalService.userRevival(userId);

        response.sendRedirect("Management");
    }
}