package keiziban.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import keiziban.service.DeletePostService;


@WebServlet(urlPatterns = { "/deletePost" })
public class DeletePostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

//get送信とpost送信のイメージは相手からの要求はget、処理を渡すのはpost。
//情報を見せてくれはGET、情報をデータベースに格納してくれはPOST。

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        int postId = (Integer.parseInt(request.getParameter("posted.id")));

        //loginServiceからユーザー情報を持ってくる。情報はuserの中にまとめられている。
        DeletePostService DeletePostService = new DeletePostService();
        DeletePostService.deletePost(postId);

        response.sendRedirect("/index.jsp");
    }
}