package keiziban.service;

import static keiziban.utils.CloseableUtil.*;
import static keiziban.utils.DBUtil.*;

import java.sql.Connection;

import keiziban.dao.UserDao;

public class UserRevivalService {

	 public void userRevival(int userId) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            UserDao userDao = new UserDao();
	            userDao.userRevival(connection, userId);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }
}
