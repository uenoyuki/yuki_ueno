package keiziban.service;

import static keiziban.utils.CloseableUtil.*;
import static keiziban.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import keiziban.beans.Post;
import keiziban.beans.UserPost;
import keiziban.dao.PostDao;
import keiziban.dao.UserPostDao;

public class PostService {

    public void register(Post posts) {

        Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            postDao.insert(connection, posts);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

    }
    private static final int LIMIT_NUM = 1000;

    public List<UserPost> getPost() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserPostDao postDao = new UserPostDao();
            List<UserPost> ret = postDao.getUserPost(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}