package keiziban.service;

import static keiziban.utils.CloseableUtil.*;
import static keiziban.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import keiziban.beans.UserComment;
import keiziban.dao.UserCommentDao;

public class UserCommentService {

    private static final int LIMIT_NUM = 1000;

    public List<UserComment> getComment() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserCommentDao commentedDao = new UserCommentDao();
            List<UserComment> ret = commentedDao.getUserComment(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}