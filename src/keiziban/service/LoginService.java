package keiziban.service;

import static keiziban.utils.CloseableUtil.*;
import static keiziban.utils.DBUtil.*;

import java.sql.Connection;

import keiziban.beans.User;
import keiziban.dao.UserDao;
import keiziban.utils.CipherUtil;

public class LoginService {

    public User login(String login_id, String password) {

        Connection connection = null;
        try {
           //データベースへ接続するための準備
            connection = getConnection();

            UserDao userDao = new UserDao();//データベースから情報をやり取りするためにdaoを準備
            String encPassword = CipherUtil.encrypt(password);//パスワードの暗号化
            User user = userDao.getUser(connection, login_id, encPassword);//実際に入力された値をデータベースから抽出する。

            //データを返す。
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}