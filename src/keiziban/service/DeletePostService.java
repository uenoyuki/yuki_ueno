package keiziban.service;

import static keiziban.utils.CloseableUtil.*;
import static keiziban.utils.DBUtil.*;

import java.sql.Connection;

import keiziban.dao.UserPostDao;

public class DeletePostService {

	 public void deletePost(int postId) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            UserPostDao userPostDao = new UserPostDao();
	            userPostDao.deletePost(connection, postId);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }
}
